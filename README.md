# VHDL library for clock entities.

This repo is a collection of files I use occasionally in other big projects.

This repo like all my other repos is following a standard of which I try to implement in all my repositories:
 - Packages are loaded in in `lib/`.
 - If you are using Quartus' IDE, then source the `*tcl` files in your `*qsf` project file.
