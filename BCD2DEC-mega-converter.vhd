library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity converter is
	generic	(
		in_width:integer:=4
	);
	port	(
		input:in std_logic_vector(in_width-1 downto 0);
		output:out std_logic_vector((integer(ceil(log10(real(2**in_width)))))*4-1 downto 0)
	);
end entity;

architecture arc of converter is
constant o_range:integer:=(integer(ceil(log10(real(2**in_width)))))*4;
--type digits is array (o_range downto 0) of integer range 0 to 9;
--signal j:integer range 0 to o_range;
begin
	conversion:for j in 0 to o_range/4-1 generate
		output((j+1)*4-1 downto j*4)<=conv_std_logic_vector(((conv_integer(input))/(10**j))mod(10**(j+1)),4);
	end generate conversion;
end arc;