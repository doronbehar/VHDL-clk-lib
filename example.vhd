library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library clock;
use clock.components.all;
use clock.utils.all;

entity example is
	port(
		reset	:in std_logic;
		adjust	:in adjust_type;
		clk		:in std_logic;
		digits	:out std_logic_vector(4*6-1 downto 0)
	);
end entity;

architecture arc of example is
	signal clk_s		:std_logic;
	signal counter		:integer range 0 to 24*60*60;
begin
	clk_s<=adjust.clk when adjust.active='0' else clk;
	process(clk_s,reset)
	begin
		if reset='1' then
			counter<=0;
		elsif rising_edge(clk_s) then
			if adjust.active='0' then
				if counter<24*60*60 then
					counter<=counter+1;
				else
					counter<=0;
				end if;
			else --adjust.active='1' then
				if adjust.down='0'and adjust.up='1' then
					counter<=counter+1;
				elsif adjust.down='1'and adjust.up='0' then
					counter<=counter-1;
				end if;
				if counter=24*60*60 then
					counter<=0;
				end if;
			end if;
		end if;
	end process;
	counter2digits:for j in 0 to 5 generate
		even:if ((j mod 2)=0) generate
			digits(4*(j+1)-1 downto j*4)<=conv_std_logic_vector(((counter/(60**(j/2)))mod 10),4);
		end generate even;
		odd:if ((j mod 2)=1) generate
			digits(4*(j+1)-1 downto j*4)<=conv_std_logic_vector(((counter/(60**(j/2)))mod 60)/10,4);
		end generate odd;
	end generate counter2digits;
end arc;
