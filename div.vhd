library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

entity div is
	generic(
		divide_by:integer range 0 to 50*10**6:=50*10**6
	);
	port(
		reset	:in std_logic;
		clkin	:in std_logic;
		clkout	:out std_logic
	);
end entity;

architecture arc of div is
	constant cntr_max:integer:=divide_by/2;
	signal clk_s:std_logic;
begin
	process(clkin,reset)
		variable cntr:integer range 0 to divide_by/2;
	begin
		if reset='1' then
			cntr:=0;
			clk_s<='0';
		elsif rising_edge(clkin) then
			if cntr<cntr_max then
				cntr:=cntr+1;
			else
				cntr:=0;
				clk_s<=not clk_s;
			end if;
		end if;
	end process;
	clkout<=clk_s;
end arc;
