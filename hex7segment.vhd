--=======================================================================
--**********************Table truth for hex7segment**********************
--=======================================================================

--=======================================================================
--					Data					||			segments
--=======================================================================
--character	|	hex(4)(	3)(	2)(	1)(	0)		||	a	b	c	d	e	f	g
--	"0"		|		0	0	0	0	0		||	0	0	0	0	0	0	1	 ___a____
--	"1"		|		0	0	0	0	1		||	1	0	0	1	1	1	1	|		 |
--	"2"		|		0	0	0	1	0		||	0	0	1	0	0	1	0	f		 b
--	"3"		|		0	0	0	1	1		||	0	0	0	0	1	1	0	|___g____|
--	"4"		|		0	0	1	0	0		||	1	0	0	1	1	0	0	|		 |
--	"5"		|		0	0	1	0	1		||	0	1	0	0	1	0	0	e		 c
--	"6"		|		0	0	1	1	0		||	0	1	0	0	0	0	0	|___d____|
--	"7"		|		0	0	1	1	1		||	0	0	0	1	1	1	1
--	"8"		|		0	1	0	0	0		||	0	0	0	0	0	0	0
--	"9"		|		0	1	0	0	1		||	0	0	0	1	1	0	0
--	"A"		|		0	1	0	1	0		||	0	0	0	1	0	0	0
--	"b"		|		0	1	0	1	1		||	1	1	0	0	0	0	0
--	"C"		|		0	1	1	0	0		||	0	1	1	0	0	0	1
--	"d"		|		0	1	1	0	1		||	1	0	0	0	0	1	0
--	"E'		|		0	1	1	1	0		||	0	1	1	0	0	0	0
--	"F"		|		0	1	1	1	1		||	0	1	1	1	0	0	0
----------------until here it's normal hex7segment encoder---------------
--	"c"		|		1	0	0	0	0		||	1	1	1	0	0	1	0
--	"H"		|		1	0	0	0	1		||	1	0	0	1	0	0	0
--	"J"		|		1	0	0	1	0		||	1	0	0	0	0	1	1
--	"L"		|		1	0	0	1	1		||	1	1	1	0	0	0	1
--	"S"		|		1	0	1	0	0		||	0	1	0	0	1	0	0
--	"o"		|		1	0	1	0	1		||	1	1	0	0	0	1	0
--	"P"		|		1	0	1	1	0		||	0	0	1	1	0	0	0
--	"U"		|		1	0	1	1	1		||	1	0	0	0	0	0	1
--	"y"		|		1	1	0	0	0		||	1	0	0	0	1	0	0
--	"-"		|		1	1	0	0	1		||	1	1	1	1	1	1	0
--	"_"		|		1	1	0	1	0		||	1	1	1	0	1	1	1
--	"="		|		1	1	0	1	1		||	1	1	1	0	1	1	0

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

entity hex7segment is
	port(
		data 		:in std_logic_vector(4 downto 0);
		segments	:out std_logic_vector(6 downto 0)
	);
end entity;

architecture logic of hex7segment is
begin
	segments<=
		"1000000" when data="00000" else	--"0"
		"1111001" when data="00001" else	--"1"
		"0100100" when data="00010" else	--"2"
		"0110000" when data="00011" else	--"3"
		"0011001" when data="00100" else	--"4"
		"0010010" when data="00101" else	--"5"
		"0000010" when data="00110" else	--"6"
		"1111000" when data="00111" else	--"7"
		"0000000" when data="01000" else	--"8"
		"0011000" when data="01001" else	--"9"
		"0001000" when data="01010" else	--"A"
		"0000011" when data="01011" else	--"B"
		"1000110" when data="01100" else	--"C"
		"0100001" when data="01101" else	--"D"
		"0000110" when data="01110" else	--"E"
		"0001110" when data="01111" else	--"F"
		"0100111" when data="10000" else	--"c"
		"0001001" when data="10001" else	--"H"
		"1100001" when data="10010" else	--"J"
		"1000111" when data="10011" else	--"L"
		"0010010" when data="10100" else	--"S"
		"0100011" when data="10101" else	--"o"
		"0001100" when data="10110" else	--"P"
		"1000001" when data="10111" else	--"U"
		"0010001" when data="11000" else	--"y"
		"0111111" when data="11001" else	--"-"
		"1110111" when data="11010" else	--"_"
		"0110111" when data="11011" else	--"="
		"1111111";
end logic;

architecture sel of hex7segment is
begin
	with data select segments<=
		"1000000" when "00000",	--0
		"1111001" when "00001",	--1
		"0100100" when "00010",	--2
		"0110000" when "00011",	--3
		"0011001" when "00100",	--4
		"0010010" when "00101",	--5
		"0000010" when "00110",	--6
		"1111000" when "00111",	--7
		"0000000" when "01000",	--8
		"0011000" when "01001",	--9
		"0001000" when "01010",	--A
		"0000011" when "01011",	--B
		"1000110" when "01100",	--C
		"0100001" when "01101",	--D
		"0000110" when "01110",	--E
		"0001110" when "01111",	--F
		"0100111" when "10000",	--"c"
		"0001001" when "10001",	--"H"
		"1100001" when "10010",	--"J"
		"1000111" when "10011",	--"L"
		"0010010" when "10100",	--"S"
		"0100011" when "10101",	--"o"
		"0001100" when "10110",	--"P"
		"1000001" when "10111",	--"U"
		"0010001" when "11000",	--"y"
		"0111111" when "11001",	--"-"
		"1110111" when "11010",	--"_"
		"0110111" when "11011",	--"="
		"1111111" when others;
end sel;

architecture proc of hex7segment is
begin
	process(data)
	begin
		case data is
			when "00000"	=>segments<="1000000";	--0
			when "00001"	=>segments<="1111001";	--1
			when "00010"	=>segments<="0100100";	--2
			when "00011"	=>segments<="0110000";	--3
			when "00100"	=>segments<="0011001";	--4
			when "00101"	=>segments<="0010010";	--5
			when "00110"	=>segments<="0000010";	--6
			when "00111"	=>segments<="1111000";	--7
			when "01000"	=>segments<="0000000";	--8
			when "01001"	=>segments<="0011000";	--9
			when "01010"	=>segments<="0001000";	--A
			when "01011"	=>segments<="0000011";	--B
			when "01100"	=>segments<="1000110";	--C
			when "01101"	=>segments<="0100001";	--D
			when "01110"	=>segments<="0000110";	--E
			when "01111"	=>segments<="0001110";	--F
			when "10000"	=>segments<="0100111";	--"c"
			when "10001"	=>segments<="0001001";	--"H"
			when "10010"	=>segments<="1100001";	--"J"
			when "10011"	=>segments<="1000111";	--"L"
			when "10100"	=>segments<="0010010";	--"S"
			when "10101"	=>segments<="0100011";	--"o"
			when "10110"	=>segments<="0001100";	--"P"
			when "10111"	=>segments<="1000001";	--"U"
			when "11000"	=>segments<="0010001";	--"y"
			when "11001"	=>segments<="0111111";	--"-"
			when "11010"	=>segments<="1110111";	--"_"
			when "11011"	=>segments<="0110111";	--"="
			when others		=>segments<="1111111";
		end case;
	end process;
end proc;

architecture RTL of hex7segment is
begin
	segments(0)<=
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))		or
		((not data(4)) and (not data(3)) and data(2) and (not data(1)) and (not data(0)))		or
		((not data(4)) and data(3) and (not data(2)) and data(1) and data(0))					or
		((not data(4)) and data(3) and data(2) and (not data(1)) and data(0))					or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and (not data(0)))		or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))				or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and (not data(0)))				or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and data(0))					or
		(data(4) and (not data(3)) and data(2) and (not data(1)) and data(0))					or
		(data(4) and (not data(3)) and data(2) and data(1) and data(0))							or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and (not data(0)))				or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and data(1) and data(0));
	segments(1)<=
		((not data(4)) and (not data(3)) and data(2) and (not data(1)) and data(0))				or
		((not data(4)) and (not data(3)) and data(2) and data(1) and (not data(0)))				or
		((not data(4)) and data(3) and (not data(2)) and data(1) and data(0))					or
		((not data(4)) and data(3) and data(2) and (not data(1)) and (not data(0)))				or
		((not data(4)) and data(3) and data(2) and data(1) and (not data(0)))					or
		((not data(4)) and data(3) and data(2) and data(1) and data(0))							or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and (not data(0)))		or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and data(0))					or
		(data(4) and (not data(3)) and data(2) and (not data(1)) and (not data(0)))				or
		(data(4) and (not data(3)) and data(2) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and data(1) and data(0));
	segments(2)<=
		((not data(4)) and (not data(3)) and (not data(2)) and data(1) and (not data(0)))		or
		((not data(4)) and data(3) and data(2) and (not data(1)) and (not data(0)))				or
		((not data(4)) and data(3) and data(2) and data(1) and (not data(0)))					or
		((not data(4)) and data(3) and data(2) and data(1) and data(0))							or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and (not data(0)))		or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and data(0))					or
		(data(4) and (not data(3)) and data(2) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and data(1) and data(0));
	segments(3)<=
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))		or
		((not data(4)) and (not data(3)) and data(2) and (not data(1)) and (not data(0)))		or
		((not data(4)) and (not data(3)) and data(2) and data(1) and data(0))					or
		((not data(4)) and data(3) and (not data(2)) and (not data(1)) and data(0))				or
		((not data(4)) and data(3) and (not data(2)) and data(1) and (not data(0)))				or
		((not data(4)) and data(3) and data(2) and data(1) and data(0))							or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))				or
		(data(4) and (not data(3)) and data(2) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0));
	segments(4)<=
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))		or
		((not data(4)) and (not data(3)) and (not data(2)) and data(1) and data(0))				or
		((not data(4)) and (not data(3)) and data(2) and (not data(1)) and (not data(0)))		or
		((not data(4)) and (not data(3)) and data(2) and (not data(1)) and data(0))				or
		((not data(4)) and (not data(3)) and data(2) and data(1) and data(0))					or
		((not data(4)) and data(3) and (not data(2)) and (not data(1)) and data(0))				or
		(data(4) and (not data(3)) and data(2) and (not data(1)) and (not data(0)))				or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and (not data(0)))				or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and data(1) and data(0));
	segments(5)<=
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))		or
		((not data(4)) and (not data(3)) and (not data(2)) and data(1) and (not data(0)))		or
		((not data(4)) and (not data(3)) and (not data(2)) and data(1) and data(0))				or
		((not data(4)) and (not data(3)) and data(2) and data(1) and data(0))					or
		((not data(4)) and data(3) and data(2) and (not data(1)) and data(0))					or
		(data(4) and (not data(3)) and (not data(2)) and (not data(1)) and (not data(0)))		or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and (not data(0)))				or
		(data(4) and (not data(3)) and data(2) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and (not data(1)) and data(0))					or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)))					or
		(data(4) and data(3) and (not data(2)) and data(1) and data(0));
	segments(6)<=
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and (not data(0)))	or
		((not data(4)) and (not data(3)) and (not data(2)) and (not data(1)) and data(0))		or
		((not data(4)) and (not data(3)) and data(2) and data(1) and data(0))					or
		((not data(4)) and data(3) and data(2) and (not data(1)) and (not data(0)))				or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and (not data(0)))				or
		(data(4) and (not data(3)) and (not data(2)) and data(1) and data(0))					or
		(data(4) and (not data(3)) and data(2) and data(1) and data(0))							or
		(data(4) and data(3) and (not data(2)) and data(1) and (not data(0)));
	------------------------------------------------------------------------------------------
	--Credits to software called "Logic Friday" which generated this code.
	--The software can be found at http://sontrak.com/ and downloaded for free (windows only).
	-------------------------------------------------------------------------------------------
end RTL;
