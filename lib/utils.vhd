library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

package utils is
	type adjust_type is record
		active:std_logic;
		up:std_logic;
		down:std_logic;
		clk:std_logic;
	end record;
	type hex_segments_type is array(7 downto 0)of std_logic_vector(6 downto 0);
	type hex_data_type is array(7 downto 0) of std_logic_vector(4 downto 0);
	type hex_type is record
		data:hex_data_type;
		segments:hex_segments_type;
	end record;
	type clk_type is record
		digits	:std_logic_vector(4*6-1 downto 0);
		clk		:std_logic;
		adjust	:adjust_type;
	end record;
end package;
