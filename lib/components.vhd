library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library clock;
use clock.utils.all;

package components is
	component div
		generic(
			divide_by:integer:=50*10**6
		);
		port(
			reset	:in std_logic;
			clkin	:in std_logic;
			clkout	:out std_logic
		);
	end component;
	component hex7segment
		port(
			data 		:in std_logic_vector(4 downto 0);
			segments	:out std_logic_vector(6 downto 0)
		);
	end component;
	component example
		port(
			reset	:in std_logic;
			adjust	:in adjust_type;
			clk		:in std_logic;
			digits	:out std_logic_vector(4*6-1 downto 0)
		);
	end component;
end package;
